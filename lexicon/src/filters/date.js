export default (value) => {
  const date = new Date(value)
  return date.toLocaleString('et-ee', { day: '2-digit', month: 'long', year: 'numeric', hour: '2-digit', minute: '2-digit' })
}
