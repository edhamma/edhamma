export function notNegative (number) {
  if (number < 0) {
    return 0
  } else {
    return number
  }
}
