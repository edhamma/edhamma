import Vue from 'vue'
import './plugins/vuetify'
import App from './App.vue'
import router from './router'
import store from './store'
import DataFilter from './filters/date'

Vue.filter('date', DataFilter)

Vue.config.productionTip = false

/* eslint-disable no-new */
export const bus = new Vue()

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
