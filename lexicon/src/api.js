import Axios from 'axios'
import Cookies from 'js-cookie'

export function createConfigWithCsrfToken () {
  var csrftoken = Cookies.get('csrftoken')
  var config = csrftoken ? { headers: { 'X-CSRFToken': csrftoken } } : null
  return config
}

export const API = {
  axios: Axios.create({ baseURL: '/api/', timeout: 10000 }),
  // GET
  getTermList (offset, limit) { return this.axios.get(`term/?offset=${offset}&limit=${limit}`) },
  getSearchResults (lookup, excludeLang) { return this.axios.get(`search/${lookup}/?excludeLang=${excludeLang}`) },
  getTermRanking (term) { return this.axios.get('term/' + term + '/ranking/') },
  getTerm (term) { return this.axios.get('term/' + term + '/') },
  getUser () { return this.axios.get('user/current/') },
  // POST
  addComment (comment) { return this.axios.post('term-comment/', comment, createConfigWithCsrfToken()) }
}
