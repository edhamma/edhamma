'use strict'

import { TERM_URL_PREFIX } from '@/constants'

export function parseEdhammaMarkdown (inputString) {
  return parseInnerLinks(inputString)
}

const INNER_LINK_REGEX = /\[([^\]]+)\]<([^\s>]+)>/gm

export function parseInnerLinks (inputString) { // TODO Do not export this function, use this https://stackoverflow.com/a/30794280/9035706 to import if for testing
  /**
   * Find the following syntax `[link text]<term>`
   * and convert it into `<a>`.
   */
  const replaceValue = `<a href="#${TERM_URL_PREFIX}$2">$1</a>`
  return inputString.replace(INNER_LINK_REGEX, replaceValue)
}
