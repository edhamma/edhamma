import slugify from 'slugify'

slugify.extend({
  'Ā': 'a',
  'ā': 'a',
  'Ī': 'i',
  'ī': 'i',
  'Ū': 'u',
  'ū': 'u',
  'Ṅ': 'n',
  'ṅ': 'n',
  'Ṃ': 'm',
  'ṃ': 'm',
  'Ñ': 'n',
  'ñ': 'n',
  'Ṭ': 't',
  'ṭ': 't',
  'Ḍ': 'd',
  'ḍ': 'd',
  'Ṇ': 'n',
  'ṇ': 'n',
  'Ḷ': 'l',
  'ḷ': 'l'
})

export default slugify
