import Vue from 'vue'
import Router from 'vue-router'

import Home from './components/Home.vue'
import Term from './components/Term'
import LetterToAdmin from './components/LetterToAdmin'
import SandBox from './components/SandBox'

import { TERM_URL_PREFIX } from './constants'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home
    },
    {
      path: `${TERM_URL_PREFIX}:routerTerm`,
      props: true,
      name: 'Term',
      component: Term
    },
    {
      path: '/liivakast/',
      name: 'SandBox',
      component: SandBox
    },
    {
      path: '/kiri-haldajale',
      name: 'LetterToAdmin',
      component: LetterToAdmin
    }
  ]
})
