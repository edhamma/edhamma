import { shallowMount, createLocalVue } from '@vue/test-utils'
import Vuetify from 'vuetify'
import Vuex from 'vuex'
import Toolbar from '@/components/Toolbar.vue'

const localVue = createLocalVue()
localVue.use(Vuetify)
localVue.use(Vuex)

const $route = {
  path: '/some/path'
}

describe('Toolbar.vue', () => {
  let getters
  let store

  it('Assert that login message is shown to an unauthenticated user', () => {
    getters = {
      user: () => { return null }
    }

    store = new Vuex.Store({
      getters
    })

    const wrapper = shallowMount(Toolbar, { store, localVue, mocks: { $route } })
    const userButton = wrapper.find('#user-btn')
    expect(userButton.text()).toMatch('Logi sisse')
  })

  it('Assert that greeting is shown to an authenticated user', () => {
    getters = {
      user: () => { return { username: 'Test User Username' } }
    }

    store = new Vuex.Store({
      getters
    })

    const wrapper = shallowMount(Toolbar, { store, localVue, mocks: { $route } })
    const userButton = wrapper.find('#user-btn')
    expect(userButton.text()).toMatch('Tere, Test User Username!')
  })
})
