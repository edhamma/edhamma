import { parseInnerLinks } from '@/lib/edhamma-markdown'
import { TERM_URL_PREFIX } from '@/constants'

describe('Test inner link parsing', () => {
  it('Test simple link syntax', () => {
    const inputString = 'Check out this [term]<term>. This [one]<another-term> is good, too.'
    const expectedOutput = `Check out this <a href="#${TERM_URL_PREFIX}term">term</a>. This <a href="#${TERM_URL_PREFIX}another-term">one</a> is good, too.`
    expect(parseInnerLinks(inputString)).toBe(expectedOutput)
  })
})
