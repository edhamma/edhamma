import { mount } from '@vue/test-utils'
import TermMetaData from '@/components/TermMetaData.vue'

describe('TermMetaData.vue', () => {
  it('Check layout for both word class and gender', () => {
    const testTerm = {
      original_word: 'test term',
      get_word_class_display: 'nimis',
      get_gender_display: 'meessugu'
    }
    const wrapper = mount(TermMetaData, { propsData: { term: testTerm } })
    const gramaticalInfo = wrapper.find('#term-metadata > p')
    expect(gramaticalInfo.text()).toMatch('(nimis, meessugu)')
  })

  it('Check layout for word class', () => {
    const testTerm = {
      original_word: 'test term',
      get_word_class_display: 'nimis',
      get_gender_display: ''
    }
    const wrapper = mount(TermMetaData, { propsData: { term: testTerm } })
    const gramaticalInfo = wrapper.find('#term-metadata > p')
    expect(gramaticalInfo.text()).toMatch('(nimis)')
  })

  it('Check layout for gender', () => {
    const testTerm = {
      original_word: 'test term',
      get_word_class_display: '',
      get_gender_display: 'meessugu'
    }
    const wrapper = mount(TermMetaData, { propsData: { term: testTerm } })
    const gramaticalInfo = wrapper.find('#term-metadata > p')
    expect(gramaticalInfo.text()).toMatch('(meessugu)')
  })
})
