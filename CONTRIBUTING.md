# Contributing guidelines

## Import Conventions

Imports should be in the following order:

1. Python standard library
2. General third party libraries (e.g. pytz)
3. Django (`django.conf` last)
4. Third party Django extensions
5. Our modules (starting with utils and common *[still to be separated]*)

## Naming Conventions

### Tests

Reusable base test cases should have suffix `TestCase` (e.g. `AdminSiteTestCase`).
They should not be executable on their own, but just provide base functionality for derived classes.
Therefore in case of test methods, use `generic_test` prefix instead of `test`.

Test classes which are directly executable by test runner should have suffix `Test`
(e.g. `ArchivedItemsDisplayTest`). To use generic test methods inherited from base class,
use the following idiom:

```py
def test_foo(self):
    self.generic_test_foo()
```

## Testing

Running tests while being in `src/` directory:

- Use `./manage.py test` to run all unittests
- Use `./manage.py test -p='f_tests*.py'` to run all functional tests in Chrome in headless mode
- Use `./manage.py test -p='f_tests*.py' --with-gui` to run all functional tests in Chrome with GUI
- Use `./manage.py test -p='*tests*.py'` to run both unittests and functional tests


## Translations

### Translations for Django

The following steps should be performed for each Django app, which has translatable content.

1. Go to the app's directory
2. Run `django-admin makemessages -l et` (`et` stands for `Estonian`, use the appropriate language code)
3. A file was created/updated with location `<app_dir>/locale/<language_code>/LC_MESSAGES/django.po`
    1. Open the fie
    2. Search for `msgstr ""`, write the translation between the quotation marks
    3. Search for `fuzzy`, these are places, where small modifications have happened and which are therefore unclear for Django. Sort it out and remove `fuzzy` flag.
    4. Save the file
4. In the app's directory run `django-admin compilemessages` to compile the messages into a binary file.
5. A `.mo` file was created/updated with location `<app_dir>/locale/<language_code>/LC_MESSAGES/django.mo`