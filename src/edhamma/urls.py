"""edhamma URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
import debug_toolbar
from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path, include
from rest_framework import routers

from general.api.views import LetterToAdminViewSet, UserViewSet
from term.api.views import CommentViewSet, SingleTermViewSet, TermListView, SearchView, SingleTermRankingView

router = routers.DefaultRouter()

router.register('letter-to-admin', LetterToAdminViewSet)
router.register('term-comment', CommentViewSet)
router.register('user', UserViewSet)  # To get current user, go to /api/user/current/

urlpatterns = [
                  path('api/', include(router.urls)),
                  path('api/search/<str:lookup>/', SearchView.as_view()),
                  path('api/term/', TermListView.as_view()),
                  path('api/term/<str:original_word>/', SingleTermViewSet.as_view()),
                  path('api/term/<str:original_word>/ranking/', SingleTermRankingView.as_view()),
                  path('__debug__/', include(debug_toolbar.urls)),
                  path('haldus/', admin.site.urls),
                  path('api-auth/', include('rest_framework.urls')),
                  path('nested_admin/', include('nested_admin.urls')),
                  path('deploy/', include('administration.deploy.urls')),

] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

# Change branding
admin.site.site_header = 'E-dhamma'
admin.site.site_title = 'E-dhamma'
admin.site.index_title = 'Tere tulemast!'
