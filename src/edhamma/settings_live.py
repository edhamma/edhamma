from django.utils.log import DEFAULT_LOGGING

import edhamma.secrets
from .settings import *

SECRET_KEY = edhamma.secrets.SECRET_KEY
DEPLOY_KEY = edhamma.secrets.DEPLOY_KEY

DEBUG = False
TEMPLATE_DEBUG = DEBUG

INSTALLED_APPS.remove('debug_toolbar')

# Assure that errors end up to Apache error logs via console output
# when debug mode is disabled
DEFAULT_LOGGING['handlers']['console']['filters'] = []

ALLOWED_HOSTS = ['edhamma.pythonanywhere.com', '217.146.67.80']
