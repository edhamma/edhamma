#!/usr/bin/env python
import argparse
import os
import sys


def main():
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "edhamma.settings")
    try:
        from django.core.management import execute_from_command_line
    except ImportError as exc:
        raise ImportError(
            "Couldn't import Django. Are you sure it's installed and "
            "available on your PYTHONPATH environment variable? Did you "
            "forget to activate a virtual environment?"
        ) from exc
    argv = parse_func_test_with_gui_argument()
    execute_from_command_line(argv)


def parse_func_test_with_gui_argument():
    command = sys.argv[1] if len(sys.argv) > 1 else None
    if command != 'test':
        return sys.argv
    parser = argparse.ArgumentParser()
    parser.add_argument('--with-gui', action='store_true')
    args, argv = parser.parse_known_args(sys.argv)
    if args.with_gui:
        os.environ['EDHAMMA_FUNC_TEST_WITH_GUI'] = 'true'
    return argv


if __name__ == "__main__":
    main()
