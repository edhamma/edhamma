from rest_framework import serializers
from ..models import Term, Meaning, Comment, Example


class CommentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Comment
        fields = '__all__'

    def to_internal_value(self, data):
        instance = super().to_internal_value(data)
        request = self.context.get("request")
        if request.user.is_authenticated:
            instance['user'] = request.user
            instance['name'] = ''
            instance['email'] = ''
        return instance


class ExampleSerializer(serializers.ModelSerializer):
    class Meta:
        model = Example
        fields = ('id', 'original', 'translation')
        depth = 1


class MeaningSerializer(serializers.ModelSerializer):
    example_set = ExampleSerializer(many=True)

    class Meta:
        model = Meaning
        fields = ('id', 'word_translation', 'eng', 'root', 'root_language',
                  'root_description', 'explanation', 'further', 'example_set')
        depth = 1


class CommentSetSerializer(serializers.ModelSerializer):
    class Meta:
        model = Comment
        fields = ('id', 'message', 'timestamp',
                  'author_name', 'author_email', 'approved')
        depth = 1


class SingleTermSerializer(serializers.ModelSerializer):
    meaning_set = MeaningSerializer(many=True)
    comment_set = CommentSetSerializer(many=True)

    class Meta:
        model = Term
        fields = ('id', 'original_word', 'get_word_class_display',
                  'get_gender_display', 'def_in_PTS_dict', 'admin_change_url', 'meaning_set', 'comment_set')
        depth = 1
