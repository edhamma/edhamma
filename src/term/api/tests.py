import json

from django.test import TestCase, Client

from term.models import Comment, Term, Meaning
from users.models import User
from utils.testutils import test_data as td


class TermListViewTest(TestCase):

    @classmethod
    def setUpTestData(cls):
        cls.term_b = Term.objects.create(original_word='B term')
        cls.term_a = Term.objects.create(original_word='A term')
        cls.term_c = Term.objects.create(original_word='C term')
        cls.term_e = Term.objects.create(original_word='E term')
        cls.term_d = Term.objects.create(original_word='D term')

    def test_get(self):
        """Test that terms are sorted in alphabetical order, not by ID"""
        self.assertEqual(self.client.get('/api/term/').json(),
                         ['A term', 'B term', 'C term', 'D term', 'E term'])

    def test_pagination(self):
        self.assertEqual(self.client.get('/api/term/?offset=1&limit=2').json(), ['B term', 'C term'])

    def test_pagination_exception(self):
        self.assertEqual(self.client.get('/api/term/?offset=INVALID&limit=INVALID').json(),
                         {'detail': 'Incorrect pagination details provided'})


class SingleTermRankingViewTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.term_b = Term.objects.create(original_word='B term')
        cls.term_a = Term.objects.create(original_word='A term')

    def test_get(self):
        response = self.client.get(f'/api/term/{self.term_a}/ranking/')
        self.assertEqual(response.json(), {'ranking': 0})

        response = self.client.get(f'/api/term/{self.term_b}/ranking/')
        self.assertEqual(response.json(), {'ranking': 1})

    def test_404(self):
        response = self.client.get(f'/api/term/INVALID_TERM/ranking/')
        self.assertEqual(response.json(), {'detail': 'Ei leidnud.'})


class SearchViewTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        for t in ['A Pali term', 'B Pali term', 'C Pali term', 'D Pali term', 'E Pali term', 'F Pali term']:
            term = Term.objects.create(original_word=t)
            Meaning.objects.create(term=term, word_translation=t + ' translation')

    def test_get(self):
        """
        Test that:
        1. Both Pali and Estonian matches are taken into account.
        2. Only 8 results are returned.
        """
        self.assertEqual(self.client.get('/api/search/term/').json(),
                         [{'lookup': 'A Pali term', 'term': 'A Pali term'},
                          {'lookup': 'B Pali term', 'term': 'B Pali term'},
                          {'lookup': 'C Pali term', 'term': 'C Pali term'},
                          {'lookup': 'D Pali term', 'term': 'D Pali term'},
                          {'lookup': 'E Pali term', 'term': 'E Pali term'},
                          {'lookup': 'F Pali term', 'term': 'F Pali term'},
                          {'lookup': 'A Pali term translation', 'term': 'A Pali term'},
                          {'lookup': 'B Pali term translation', 'term': 'B Pali term'}]
                         )

    def test_search_by_ascii_representation(self):
        Term.objects.create(original_word='aḍḍha')  # `aḍḍha` means opulent
        self.assertEqual(self.client.get('/api/search/addha/').json(),
                         [{'lookup': 'aḍḍha', 'term': 'aḍḍha'}])


class SingleTermViewSetTest(TestCase):
    "Tests for '/api/single-term/<original_word>' endpoint"

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.term = td.add_term()

    def test_admin_change_url(self):
        response = self.client.get(f'/api/term/{self.term.original_word}/')
        data = json.loads(response.content, encoding='utf-8')
        admin_change_url = data['admin_change_url']
        self.assertEqual(admin_change_url, '/haldus/term/term/1/change/')


class CommentAPITest(TestCase):
    "Tests for '/api/term-comment' endpoint"

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.term = td.add_term()
        cls.password = 'User__password'
        cls.user = User.objects.create_user(
            'User__username', 'User__@email.ee', cls.password)

    def test_creation_for_comment_by_anonymuse_user(self):
        client = Client()
        "Anonymuse user must provide their name and email"
        response = client.post('/api/term-comment/', {
            "name": 'Comment__name',
            "email": 'Comment__@email.ee',
            "message": 'Comment__message',
            "term": self.term.id,
        })
        self.assertAlmostEqual(response.status_code, 201)

    # FIXME This test should not pass, since the user is neither logged in nor has provided 'name' and 'email'
    def test_validation_error_for_comment_creation(self):
        "Logged in user does not need to provide their credentials, they are added by the serializer"
        client = Client()
        # self.client.login(username=self.user.username, password=self.password)
        response = self.client.post('/api/term-comment/', {
            "message": 'Comment__message',
            "term": self.term.id,
        })
        self.assertEqual(response.status_code, 201)  # FIXME Should fail

    def test_creation_for_comment_by_logged_in_user(self):
        "Logged in user does not need to provide their credentials, they are added by the serializer"
        client = Client()
        client.login(username=self.user.username, password=self.password)
        response = client.post('/api/term-comment/', {
            "message": 'Comment__message',
            "term": self.term.id,
            "name": 'Misleading name',
            "email": 'Misleading@email.ee'
        })
        self.assertEqual(response.status_code, 201)

        comment_id = json.loads(response.content.decode(
            'utf-8'), encoding='utf-8')['id']
        comment = Comment.objects.get(id=comment_id)
        self.assertEqual(comment.name, '')
        self.assertEqual(comment.email, '')
        self.assertEqual(comment.user, self.user)
