from django.db.models import Prefetch, Q
from rest_framework import viewsets, generics
from rest_framework.exceptions import ParseError, NotFound
from rest_framework.response import Response
from rest_framework.views import APIView

from utils.slugify import replace_pali_letters
from .serializers import CommentSerializer, SingleTermSerializer
from ..models import Comment, Term, Meaning


class TermListView(APIView):
    permission_classes = ()
    authentication_classes = ()

    def get(self, request, format=None):
        qs = Term.objects.all().only('original_word')
        # Pagination is implemented manually,
        # because out-of-the-box pagination is available only for generic API views, which also use serializers,
        # which we do not wnt to do.
        if 'offset' in request.query_params and 'limit' in request.query_params:
            try:  # TODO Handle list index out of range
                offset = int(request.query_params['offset'])
                limit = int(request.query_params['limit'])
                qs = qs[offset:offset + limit]
            except:
                raise ParseError('Incorrect pagination details provided')
        return Response([term.original_word for term in qs])


class SearchView(APIView):
    permission_classes = ()
    authentication_classes = ()

    def _compile_match(self, term, lookup) -> dict:
        return {'term': term, 'lookup': lookup}

    def get(self, request, lookup: str, format=None):
        lookup = replace_pali_letters(lookup)
        excluded_language = request.GET.get('excludeLang')
        if excluded_language == 'pl':
            terms = []
        else:
            terms = Term.objects.filter(Q(original_word__icontains=lookup) | Q(ascii__icontains=lookup)) \
                        .only('original_word')[:8]

        if excluded_language == 'est':
            meanings = []
        else:
            meanings = Meaning.objects.filter(word_translation__icontains=lookup) \
                           .prefetch_related('term').only('word_translation', 'term__original_word')[:8]
        matches_by_term = [self._compile_match(term.original_word, term.original_word) for term in terms]
        matches_by_meaning = [self._compile_match(meaning.term.original_word, meaning.word_translation)
                              for meaning in meanings]
        return Response((matches_by_term + matches_by_meaning)[:8])


class SingleTermRankingView(APIView):
    permission_classes = ()  # TODO Configure them globally
    authentication_classes = ()

    def get(self, request, original_word: str, format=None):  # TODO TEST
        try:
            term = Term.objects.get(original_word=original_word)
        except Term.DoesNotExist:
            raise NotFound()
        return Response({'ranking': term.ranking})


class SingleTermViewSet(generics.RetrieveAPIView):
    queryset = Term.objects.all()
    serializer_class = SingleTermSerializer
    lookup_field = 'original_word'

    def get_queryset(self):
        return super().get_queryset().prefetch_related(
            Prefetch('meaning_set', queryset=Meaning.objects.prefetch_related('example_set')),
            Prefetch('comment_set', queryset=Comment.objects.select_related('user'))
        )


class CommentViewSet(viewsets.ModelViewSet):
    # Allows access to all requests including unauthenticated ones.
    permission_classes = ()
    queryset = Comment.objects.all()
    serializer_class = CommentSerializer
