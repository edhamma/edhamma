from time import sleep
from time import sleep
from selene.api import *

from utils.testutils import AdminSiteTestCase, LexiconTestCase
from utils.testutils import test_data as td
from term.models import Term

class TermAdminTest(AdminSiteTestCase):

    def test_term_creation(self):
        browser.open_url('term/term/add/')
        s('#id_original_word').set('original').press_enter()
        # TODO Make it work with other translations as well
        s('#container > ul > li').should(have.exact_text(
            'Termin "original" lisamine õnnestus.'))
    

class AuthenticatedUserCommentingTest(LexiconTestCase):

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.term = td.add_term()


    def test_comment_adding_for_authenticated_user(self):
        self.login(direct_to=f'/index.html/#/terminid/{self.term.original_word}')
        # TODO Do not understand, why login(direct_to=...) is not working
        browser.open_url(f'/index.html/#/terminid/{self.term.original_word}')
        # Submit comment
        s('#message').set('Test message')
        s('#submit-comment').click()
        # Assert that comment is displayed
        s('#c0').should(be.visible)
        # Assert that comment is saved to db
        self.assertEqual(len(self.term.comment_set.all()), 1)

class UnauthenticatedUserCommentingTest(LexiconTestCase):

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.term = td.add_term()


    def test_comment_adding_for_unauthenticated_user(self):
        browser.open_url(f'/index.html/#/terminid/{self.term.original_word}')
        # Submit comment
        s('#name').set('Test Person name')
        s('#email').set('TestPerson@email.ee')
        s('#message').set('Test message')
        s('#submit-comment').click()
        # Assert that comment is displayed
        s('#c0').should(be.visible)
        # Assert that comment is saved to db
        self.assertEqual(len(self.term.comment_set.all()), 1)
