import json
from types import SimpleNamespace as Object

from django.conf import settings
from django.core.exceptions import ValidationError
from django.db import models
from django.utils.text import slugify
from django.utils.translation import gettext_lazy as _

from utils.adminsite import get_obj_admin_change_url
from utils.slugify import slugify_term_for_ordering


class _Gender:
    MASCULINE = 'm'
    FEMINE = 'f'
    NEUTER = 'n'
    CHOICES = (
        (MASCULINE, 'meesssgu'),
        (FEMINE, 'naissugu'),
        (NEUTER, 'kesksugu'),
    )


class _WordClass:
    VERB = 'v'
    NOUN = 'n'
    ADJECTIVE = 'a'
    ADVERB = 'd'
    CHOICES = (
        (VERB, 'tegus'),
        (NOUN, 'nimis'),
        (ADJECTIVE, 'omds'),
        (ADVERB, 'määrs')
    )


class Term(models.Model):
    original_word = models.CharField(_('original word'), max_length=250, unique=True)
    slug = models.SlugField(max_length=250,
                            help_text="Used to sort terms in alphabetical order. "
                                      "Pali specific unicode characters are not sorted correctly, "
                                      "so we an ASCII representation is created, which includes numbers for "
                                      "ordering Pali letters. "
                                      "NB: `slug` is not necessarily unique, although in most cases it should be.")
    ascii = models.SlugField(max_length=250)
    gender = models.CharField(_('gender'),  # TODO Do others as this one
                              max_length=1, choices=_Gender.CHOICES, blank=True)
    word_class = models.CharField(_('word class'),
                                  max_length=1, choices=_WordClass.CHOICES, blank=True)
    def_in_PTS_dict = models.TextField(
        blank=True, verbose_name=_('definition in PTS dictionary'))
    auto_imported = models.BooleanField(default=False)
    _translations = models.TextField(blank=True,
                                     help_text='A cached JSON list compound from the'
                                               '`word_translation` fields of all associated `Meaning`s. '
                                               'Used by admin search and by API search endpoint.')
    last_modified = models.DateField(auto_now=True)

    class Meta:
        verbose_name = _('term')
        verbose_name_plural = _('terms')
        ordering = ['slug']

    def save(self, *args, **kwargs):
        self.ascii = slugify(self.original_word)
        self.slug = slugify_term_for_ordering(self.original_word)
        super().save(*args, **kwargs)

    def __str__(self):
        return self.original_word

    def renew_translations(self):
        self._translations = json.dumps([m.word_translation for m in self.meaning_set.all()])
        self.save()

    @property
    def ranking(self):
        return self.__class__.objects.filter(slug__lt=self.slug).count()

    @property
    def admin_change_url(self):
        return get_obj_admin_change_url(self)

    def get_absolute_url(self):  # This is needed for "vaata leksikonis >" button is admin
        return f'/#/terminid/{self.original_word}'


class _RootLang:
    PL = 'p'
    SKR = 's'
    CHOICES = (
        (PL, 'paali'),
        (SKR, 'sanskrit'),
    )


YET_TO_BE_TRANSLATED = 'TÕLKIMISEL'


class Meaning(models.Model):
    term = models.ForeignKey(
        Term, on_delete=models.CASCADE, verbose_name=_('term'))
    word_translation = models.CharField(max_length=250, verbose_name=_('in Estonian'))
    eng = models.CharField(max_length=250, blank=True,
                           verbose_name=_('in English'))
    root = models.CharField(max_length=100, blank=True, verbose_name=_('root'))
    root_language = models.CharField(
        max_length=1, choices=_RootLang.CHOICES, blank=True, verbose_name=_('root language'))
    root_description = models.CharField(
        max_length=100, blank=True, verbose_name=_('root description'))
    explanation = models.TextField(blank=True, verbose_name=_('explanation'))
    further = models.TextField(
        blank=True, verbose_name=_('further explanation'))

    class Meta:
        verbose_name = _('meaning')
        verbose_name_plural = _('meanings')

    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)
        self.term.renew_translations()
        if self.word_translation != YET_TO_BE_TRANSLATED:
            self.term.auto_imported = False
            self.term.save()


class Example(models.Model):
    term_meaning = models.ForeignKey(Meaning, on_delete=models.CASCADE)
    original = models.TextField(verbose_name=_('original'))
    translation = models.TextField(verbose_name=_('translation'))

    def __str__(self):
        return self.original + ' -> ' + self.translation

    class Meta:
        verbose_name = _('example')
        verbose_name_plural = _('examples')


class Comment(models.Model):
    term = models.ForeignKey(Term, on_delete=models.CASCADE)
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL, on_delete=models.PROTECT, blank=True, null=True, verbose_name=_('user'), )
    name = models.CharField(_('name'), max_length=200, blank=True)
    email = models.EmailField(_('email'), blank=True, null=True)
    message = models.TextField(_('content'))
    timestamp = models.DateTimeField(
        auto_now_add=True, verbose_name=_('timestamp'))
    approved = models.BooleanField(default=True, verbose_name=_('approved'))

    def __str__(self):
        return self.author.name + ': ' + self.message

    class Meta:
        verbose_name = _('comment')
        verbose_name_plural = _('comments')

    def clean(self):
        """
        If the comment is added by an unauthenticated person,
        they must type their name and email.
        If the comment is added by a logged in user, name and email are not needed,
        for the comment will be linked with the User object, which already proveides
        username and email.
        """
        if not (self.user or (self.name and self.email)):
            raise ValidationError(
                "Either 'user' or both 'name' and 'email' must be provided.")

    @property
    def author(self):
        # TODO In order to not add extra methods to admin, use separated author_name and author_email properties
        """
        Since the author of the comment can be determined
        either by 'name' & 'email' or by 'user', use this methor to access author data.
        """
        if self.user:
            return Object(
                name=self.user.username,
                email=self.user.email
            )
        return Object(
            name=self.name,
            email=self.email
        )

    # Theese methods are used for admin and for serialization 
    def author_name(self):
        return self.author.name

    author_name.short_description = _("author's name")

    def author_email(self):
        return self.author.email

    author_email.short_description = _("author's email")
