import nested_admin
from django.contrib import admin
from django.db import models
from django.forms import TextInput, Textarea

from .models import Term, Meaning, Example, Comment

NEW_SIZES = {
    models.CharField: {'widget': TextInput(attrs={'size': '25'})},
    models.TextField: {'widget': Textarea(attrs={'rows': 5, 'cols': 80})},
}


class ExampleAdmin(nested_admin.NestedStackedInline):
    model = Example
    inlines = ()
    extra = 0
    formfield_overrides = {models.TextField: {
        'widget': Textarea(attrs={'rows': 2, 'cols': 80})},
    }


class MeaningAdmin(nested_admin.NestedStackedInline):
    model = Meaning
    extra = 0
    fields = ('word_translation', 'eng', ('root', 'root_language', 'root_description'),
              ('explanation', 'further'))
    inlines = (ExampleAdmin,)
    formfield_overrides = NEW_SIZES


class CommentAdmin(nested_admin.NestedStackedInline):
    model = Comment
    extra = 0
    readonly_fields = ['author_name', 'author_email', 'user', 'message']
    fields = (('author_name', 'author_email', 'user'), 'message')
    formfield_overrides = NEW_SIZES


@admin.register(Term)  # TODO Make sure, that all admin classes use that decorator, I had problems regarding hot reload using `admin.site.register`
class TermAdmin(nested_admin.NestedModelAdmin):
    # when Model.get_absolute_url is present,
    # a button is added to admin site.
    # Since I want to create the button manually from a template
    # (because this feature seems to work only if get_absolute_url refares to a Django view),
    # this feature is explicitly disabled.
    view_on_site = False
    save_on_top = True
    fields = (('original_word'),
              ('word_class', 'gender'), 'def_in_PTS_dict')
    search_fields = ('original_word', 'ascii', '_translations')
    inlines = (MeaningAdmin, CommentAdmin)
    list_display = ('original_word', 'meanings')
    list_filter = ('auto_imported', )
    change_form_template = 'term/change_form.html'
    formfield_overrides = NEW_SIZES

    def get_queryset(self, request):
        return super().get_queryset(request).prefetch_related('meaning_set')

    def meanings(self, obj) -> str:  # TODO add translations to Estonian
        return '; '.join((t.word_translation for t in obj.meaning_set.all()))


# -----


class ApproveCommentAdmin(admin.ModelAdmin):
    save_on_top = True
    list_display = ('term', 'message', 'approved')
    list_editable = ('approved',)
    list_filter = ('approved', 'timestamp')


admin.site.register(Comment, ApproveCommentAdmin)
