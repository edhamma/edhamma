# Generated by Django 2.0.1 on 2018-11-18 21:13

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('term', '0011_auto_20181117_2345'),
    ]

    operations = [
        migrations.RenameField(
            model_name='comment',
            old_name='content',
            new_name='message',
        ),
        migrations.RemoveField(
            model_name='comment',
            name='author',
        ),
        migrations.AddField(
            model_name='comment',
            name='name',
            field=models.CharField(default='default', max_length=200, verbose_name='name'),
            preserve_default=False,
        ),
    ]
