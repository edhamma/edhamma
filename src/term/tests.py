from django.test import TestCase
from users.models import User
from django.core.exceptions import ValidationError

from utils.testutils.test_data import add_term
from .models import Comment, Term


class TermTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.term_c = Term.objects.create(original_word='ca')
        cls.term_ā = Term.objects.create(original_word='āa')
        cls.term_a = Term.objects.create(original_word='aa')
        cls.term_g = Term.objects.create(original_word='ga')


    def test_ordering(self):
        """
        Test that terms are sorted in alphabetical order and
        not ASCII Pali letters are not simply put to the end of the list.
        """
        terms_in_alphabetical_order = (self.term_ā, self.term_a, self.term_c, self.term_g)

        self.assertEqual(tuple(Term.objects.all()), terms_in_alphabetical_order)

    def test_ranking(self):
        self.assertEqual(self.term_ā.ranking, 0)
        self.assertEqual(self.term_a.ranking, 1)
        self.assertEqual(self.term_c.ranking, 2)
        self.assertEqual(self.term_g.ranking, 3)


class CommentTest(TestCase):

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.term = add_term()
        cls.user = User.objects.create_user(
            'User__username', 'User__@email.ee', 'User__password')

    def test_validation_error_if_author_name_and_email_are_not_provided(self):
        "See Comment.clean.__doc__ for explanation"
        comment = Comment.objects.create(
            term=self.term,
            message='Comment__message'
        )
        with self.assertRaises(ValidationError):
            comment.full_clean()

        comment.name = 'Comment__name'
        # Because 'email' is still missing
        with self.assertRaises(ValidationError):
            comment.full_clean()

        comment.email = 'Comment__@email.ee'
        comment.full_clean()  # Now it should pass

    def test_no_validation_error_if_user_is_provided(self):
        "See Comment.clean.__doc__ for explanation"
        comment = Comment.objects.create(
            term=self.term,
            message='Comment__message',
            user=self.user
        )
        comment.full_clean()

    def test_author_property(self):
        "See Comment.author.__doc__ for explanation"
        comment_with_name = Comment.objects.create(
            term=self.term,
            message='Comment__message',
            name='Comment__name',
            email='Comment__@email.ee'
        )
        self.assertEqual(comment_with_name.author.name, 'Comment__name')
        self.assertEqual(comment_with_name.author.email, 'Comment__@email.ee')

        comment_with_user = Comment.objects.create(
            term=self.term,
            message='Comment__message',
            user=self.user
        )
        self.assertEqual(comment_with_user.author.name, 'User__username')
        self.assertEqual(comment_with_user.author.email, 'User__@email.ee')
