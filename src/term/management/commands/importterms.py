from __future__ import annotations
import json
from dataclasses import dataclass

from django.core.management.base import BaseCommand
from django.db.utils import IntegrityError

from ...models import Term, Meaning, YET_TO_BE_TRANSLATED


class Command(BaseCommand):
    @dataclass
    class Summary:
        command: Command
        _added_terms_count: int = 0
        _already_present_terms_count: int = 0

        def _report_current_status(self):
            total = self._added_terms_count + self._already_present_terms_count
            if not total % 100:
                self.command.stdout.write(self.command.style.SUCCESS(
                    f'First {total} terms processed'))

        def report(self):
            self.command.stdout.write(self.command.style.SUCCESS(
                '---\n'
                f'{self._added_terms_count} terms were added;'
                f'{self._already_present_terms_count} were already present.'))

        def added(self):
            self._added_terms_count += 1
            self._report_current_status()

        def rejected(self):
            self._already_present_terms_count += 1
            self._report_current_status()

    help = 'Import terms from JSON file'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.summary = self.Summary(self)

    def add_arguments(self, parser):
        parser.add_argument('source_file', type=str)

    def handle(self, *args, **options):
        with open(options['source_file']) as f:
            for term_block in json.load(f):
                try:
                    term = Term(original_word=term_block['term'],
                                def_in_PTS_dict=term_block['description'], auto_imported=True)
                    term.save()
                    self._generate_meaning(term)
                    self.summary.added()
                except IntegrityError:
                    self.summary.rejected()
        self.summary.report()

    @staticmethod
    def _generate_meaning(term) -> Meaning:
        default_word_translation = YET_TO_BE_TRANSLATED
        default_explanation = 'Me ei ole seda terminit veel tõlkida jõudnud.\n' \
                              'Alt poolt leiad kommentaaride lisamise kasti,\n' \
                              'kuhu on oodatud kõik tõlkevasteid puudutavad ettepanekud.'
        meaning = Meaning(term=term, word_translation=default_word_translation,
                          explanation=default_explanation)
        meaning.save()
        return meaning
