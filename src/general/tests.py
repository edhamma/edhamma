import subprocess
import os
import re

from django.test import TestCase, Client
from django.conf import settings
from users.models import User

from utils.opsys import change_dir

OUTPUT_REGEX = re.compile(r"Backup 'db-backup-at-(\d{4}-\d{2}-\d{2})-\d{6}.sqlite3' was created successfully")


class UserDataAPIEndpointTest(TestCase):

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.password = 'User__password'
        cls.user = User.objects.create_user('User__username', 'User__@email.io', cls.password)
        cls.client = Client()

    def test_permission_denied(self):
        """
        Test PermissionDenied exception
        if unauthenticated user tries to access user data
        """
        response = self.client.get('/api/user/current/')
        self.assertEqual(response.status_code, 403)

    def test_success(self):
        self.client.login(username=self.user.username, password=self.password)
        response = self.client.get('/api/user/current/')
        self.assertEqual(response.status_code, 200)
        data = response.content.decode('utf-8')
        expected_data = '{"username":"User__username","email":"User__@email.io","is_staff":false}'
        self.assertEqual(data, expected_data)