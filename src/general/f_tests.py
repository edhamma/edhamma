from time import sleep
from selene.api import *
from utils.testutils import LexiconTestCase


class AuthenticationTest(LexiconTestCase):

    def test_login_and_logout(self):
        browser.open_url('/index.html')
        # TODO add id to this element
        LOGIN_MESSAGE = 'LOGI SISSE'
        s('#app > div.application--wrap > nav > div > div.v-toolbar__items > button > div').should(
            have.exact_text(LOGIN_MESSAGE))
        self.login()
        s('#app > div.application--wrap > nav > div > div.v-toolbar__items > button > div').should(
            have.exact_text(f'TERE, {self.user.username.upper()}!'))
        self.logout()
        s('#app > div.application--wrap > nav > div > div.v-toolbar__items > button > div').should(
            have.exact_text(LOGIN_MESSAGE))
