from users.models import User

from rest_framework import viewsets, mixins, permissions

from ..models import LetterToAdmin
from .serializers import LetterToAdminSerializer, UserSerializer


class LetterToAdminViewSet(viewsets.ModelViewSet):
    # Allows access to all requests including unauthenticated ones.
    permission_classes = ()
    queryset = LetterToAdmin.objects.all()
    serializer_class = LetterToAdminSerializer


class UserViewSet(mixins.RetrieveModelMixin, viewsets.GenericViewSet):
    permission_classes = (permissions.IsAuthenticated,)
    queryset = User.objects.all()
    serializer_class = UserSerializer

    def get_object(self):
        return self.request.user
