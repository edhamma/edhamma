from rest_framework import serializers
from users.models import User
from ..models import LetterToAdmin


class LetterToAdminSerializer(serializers.ModelSerializer):
    class Meta:
        model = LetterToAdmin
        fields = '__all__'


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('username', 'email', 'is_staff')
