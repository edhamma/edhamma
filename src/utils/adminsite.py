from django.urls import reverse


def get_obj_admin_change_url(obj):
    return reverse(f'admin:{obj._meta.app_label}_{obj._meta.model_name}_change',  args=[obj.id])
