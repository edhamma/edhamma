from django.utils.text import slugify


def slugify_term_for_ordering(term: str) -> str:
    """
    Create a representation of `term` which can be ordered alphabetically.
    By default all not ASCII characters are sorted after ASCII, so `ā` is not next to `a` as it should.
    To overcome it, an ASCII representation is created,
    where number is added behind the ASCII representation of a not ASCII character
    to guarantee that they are sorted next to each other (so that all `ā`s are together and not mixed up with `a`s)
    """
    equivalents = {'ā': 'a1',
                   'ī': 'i1',
                   'ū': 'u1',
                   'ṃ': 'm1',
                   'ṅ': 'n1',
                   'ñ': 'n2',
                   'ṭ': 't1',
                   'ḍ': 'd1',
                   'ṇ': 'n3',
                   'ḷ': 'l1'
                   }
    for key, value in equivalents.items():
        term = term.replace(key, value)
    return slugify(term)


def replace_pali_letters(word: str) -> str:
    equivalents = {'ā': 'a',
                   'ī': 'i',
                   'ū': 'u',
                   'ṃ': 'm',
                   'ṅ': 'n',
                   'ñ': 'n',
                   'ṭ': 't',
                   'ḍ': 'd',
                   'ṇ': 'n',
                   'ḷ': 'l'
                   }
    for key, value in equivalents.items():
        word = word.replace(key, value)
    return word
