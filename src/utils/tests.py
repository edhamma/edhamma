from unittest import TestCase

from utils.slugify import slugify_term_for_ordering


class SlugifyTest(TestCase):

    def test_slugify_term_for_ordering(self):
        # The word `patissuṇāṭi` means to assent, promise, agree
        self.assertEqual(slugify_term_for_ordering('patissuṇāṭi'), 'patissun3a1t1i')
