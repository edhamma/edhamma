from term.models import Term


def add_term(original_word='Term__original_word'):
    return Term.objects.create(
        original_word=original_word,
    )
