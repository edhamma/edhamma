from users.models import User


def add_user(username='test-user', psw='userspsw', is_staff=True, is_superuser=True):
    user = User.objects.create_user(username, f'{username}@test.io', psw)
    user.is_staff = is_staff
    user.is_superuser = is_superuser
    user.save()
    user.password = psw # Django does not store passwords like this, but it is handy for testing.
    return user
