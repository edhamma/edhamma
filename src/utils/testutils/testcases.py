import os

from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from django.conf import settings

from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException
from selene.api import *

from utils.testutils import test_data as td


class FunctionalTestCase(StaticLiveServerTestCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.configureBrowserDriver()

    def setUp(self):
        super().setUp()
        self.acceptAlert()

    @classmethod
    def tearDownClass(cls):
        cls.acceptAlert()
        # Avoid 'ConnectionResetError: [WinError 10054] An existing connection was forcibly closed'
        # See https://code.djangoproject.com/ticket/22252
        browser.driver().refresh()
        browser.quit()
        super().tearDownClass()

    @staticmethod
    def configureBrowserDriver():
        options = webdriver.ChromeOptions()
        # Avoid Chrome verbose warnings in output
        options.add_argument('--log-level=3')
        # Run headless unless '--with-gui' command-line argument is given
        if not os.getenv('EDHAMMA_FUNC_TEST_WITH_GUI'):
            options.add_argument('--headless')
            options.add_argument('--window-size=1280,1024')
            # next two options are required for running Chrome in Docker
            options.add_argument('--disable-dev-shm-usage')
            options.add_argument('--no-sandbox')
        driver = webdriver.Chrome(chrome_options=options)
        browser.set_driver(driver)

    @staticmethod
    def acceptAlert():
        "Unsaved changes confirmation alert must be accepted to leave page"
        the_browser = browser.driver()
        try:
            WebDriverWait(the_browser, 1).until(EC.alert_is_present(),
                                                'Timed out waiting for confirmation popup to appear')
            alert = the_browser.switch_to.alert
            alert.accept()
        except TimeoutException:
            pass


class LexiconTestCase(FunctionalTestCase):

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.user = td.add_user()

    def setUp(self):
        super().setUp()
        config.app_host = self.live_server_url

    def login(self, direct_to: str = '/index.html') -> None:
        browser.open_url(settings.LOGIN_URL + f'?next={direct_to}')
        s('#id_username').set(self.user.username)
        s('#id_password').set(self.user.password).press_enter()

    def logout(self, direct_to: str = '/index.html') -> None:
        browser.open_url(settings.ADMIN_BASE_URL + f'logout?next={direct_to}')


class AdminSiteTestCase(FunctionalTestCase):

    def setUp(self):
        super().setUp()
        config.app_host = self.live_server_url + settings.ADMIN_BASE_URL
        browser.open_url('')
        # Create user and log in
        user = td.add_user()
        s('#id_username').set(user.username)
        s('#id_password').set(user.password).press_enter()
