# E-dhamma

The source code of the [Pali-Estonian dictionary](https://edhamma.pythonanywhere.com/#/).

## Technology stack

* backend -- Django, Python
* frontend -- Vue.js, Vuetify.js

Distributed under MIT licence.